/*
  ==============================================================================

    Oscillator.h
    Created: 3 Jun 2018 1:02:25pm
    Author:  Paul van Vulpen

  ==============================================================================
*/

#pragma once

const static double pi = 3.14159265358979323846;

/*
 * This class contains functionality to
 * generates a sine wave sample with a resolution
 * based on the sample rate.
 * The state of the phase is preserved for each call.
 */
class Oscillator {
private:
	const double tau = 2 * pi;
	const double minimum_frequency = 0.1;
	const int minimum_sample_rate = 44'100;

	double frequency = 30;
    int sample_rate = 44'100;
    double phase = 0;
    double phase_step_size;
	
    void set_phase_step_size();

public:
    Oscillator() {
        set_phase_step_size();
    }

    void set_frequency(double freq);
    void set_sample_rate(int srate);
    void set_phase(double ph);
    double get_phase();

    //returns a sample based on the phase of the previous sample.
    double generate_one_sample();
};
