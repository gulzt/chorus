/*
  ==============================================================================

    Oscillator.cpp
    Created: 3 Jun 2018 1:02:25pm
    Author:  SEvanPau

  ==============================================================================
*/

#include "Oscillator.h"
#include <cmath>

double Oscillator::generate_one_sample() {
    double result = sin(phase);
    phase += phase_step_size;
    phase = fmod(phase, tau);
    return result;
}

void Oscillator::set_sample_rate(int srate) {
    sample_rate = srate >= minimum_sample_rate ? srate : minimum_sample_rate;
	set_phase_step_size();
}

void Oscillator::set_frequency(double freq) {
    frequency = freq > minimum_frequency ? freq : minimum_frequency;
    set_phase_step_size();
}

void Oscillator::set_phase_step_size() {
    phase_step_size = tau * frequency / sample_rate;
}

void Oscillator::set_phase(double ph) {
    phase = fmod(ph, tau);
}

double Oscillator::get_phase() {
    return phase;
}