/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "Oscillator.h"
#include <array>
//==============================================================================
/**
*/
class ChorusAudioProcessor  : public AudioProcessor
{
public:
    //==============================================================================
    ChorusAudioProcessor();
    ~ChorusAudioProcessor();

    //==============================================================================
    void prepareToPlay (double sampleRate, int samplesPerBlock) override;
    void releaseResources() override;

   #ifndef JucePlugin_PreferredChannelConfigurations
    bool isBusesLayoutSupported (const BusesLayout& layouts) const override;
   #endif

    void processBlock (AudioBuffer<float>&, MidiBuffer&) override;

    //==============================================================================
    AudioProcessorEditor* createEditor() override;
    bool hasEditor() const override;

    //==============================================================================
    const String getName() const override;

    bool acceptsMidi() const override;
    bool producesMidi() const override;
    bool isMidiEffect() const override;
    double getTailLengthSeconds() const override;

    //==============================================================================
    int getNumPrograms() override;
    int getCurrentProgram() override;
    void setCurrentProgram (int index) override;
    const String getProgramName (int index) override;
    void changeProgramName (int index, const String& newName) override;

    //==============================================================================
    void getStateInformation (MemoryBlock& destData) override;
    void setStateInformation (const void* data, int sizeInBytes) override;

private:
    Oscillator lfo;
    AudioSampleBuffer delayBuffer;
    int delayWritePointer {0};
    float oneMsDelay; //samplerate / 1000
    int tenMsDelay; //samplerate / 100
    int twentyMsDelay; //2 * tenMsDelay

    //Todo: define magic constant
    std::array<float, 4> delayReadPointer; //maximum number of voices

    AudioParameterFloat* speedParameter {nullptr};
    AudioParameterFloat* depthParameter {nullptr};
    AudioParameterInt* voicesParameter {nullptr};

    void wrapAround(float& samplePointer, AudioSampleBuffer& asBuffer);
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ChorusAudioProcessor)
};
