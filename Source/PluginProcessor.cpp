/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"
#include <cmath>
//==============================================================================
ChorusAudioProcessor::ChorusAudioProcessor()
#ifndef JucePlugin_PreferredChannelConfigurations
     : AudioProcessor (BusesProperties()
                     #if ! JucePlugin_IsMidiEffect
                      #if ! JucePlugin_IsSynth
                       .withInput  ("Input",  AudioChannelSet::stereo(), true)
                      #endif
                       .withOutput ("Output", AudioChannelSet::stereo(), true)
                     #endif
                       )
#endif
{
    //expose three parameters
    speedParameter = new AudioParameterFloat {"SpeedID", "Speed", 0.1f, 3.0f, 1.0f};
    depthParameter = new AudioParameterFloat {"DepthID", "Depth", 1.0f, 10.0f, 4.0f};
    voicesParameter = new AudioParameterInt {"VoicesID", "Voices", 1, 4, 1};

    //managedParameters takes ownership of raw pointers.
    addParameter(speedParameter);
    addParameter(depthParameter);
    addParameter(voicesParameter);
}

ChorusAudioProcessor::~ChorusAudioProcessor()
{
}

//==============================================================================
const String ChorusAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool ChorusAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool ChorusAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool ChorusAudioProcessor::isMidiEffect() const
{
   #if JucePlugin_IsMidiEffect
    return true;
   #else
    return false;
   #endif
}

double ChorusAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int ChorusAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int ChorusAudioProcessor::getCurrentProgram()
{
    return 0;
}

void ChorusAudioProcessor::setCurrentProgram (int index)
{
}

const String ChorusAudioProcessor::getProgramName (int index)
{
    return {};
}

void ChorusAudioProcessor::changeProgramName (int index, const String& newName)
{
}

//==============================================================================
void ChorusAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    // Use this method as the place to do any pre-playback
    // initialisation that you need..

    //setup delay timers based on the sample rate.
    tenMsDelay = static_cast<int>(sampleRate)/100;
    oneMsDelay = tenMsDelay/10.0f;
    twentyMsDelay = 2 * tenMsDelay;

    //initialise delay buffer accounting for 30ms delay
    delayBuffer.setSize(2, twentyMsDelay + tenMsDelay + samplesPerBlock);
    delayBuffer.clear();

    //setup the lfo. Other parameters are set through the GUI.
    lfo.set_sample_rate(static_cast<int>(sampleRate));
}

void ChorusAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool ChorusAudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
  #if JucePlugin_IsMidiEffect
    ignoreUnused (layouts);
    return true;
  #else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    if (layouts.getMainOutputChannelSet() != AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
   #if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
   #endif

    return true;
  #endif
}
#endif

void ChorusAudioProcessor::processBlock (AudioBuffer<float>& buffer, MidiBuffer& midiMessages)
{
    ScopedNoDenormals noDenormals;
    auto totalNumInputChannels  = getTotalNumInputChannels();
    auto totalNumOutputChannels = getTotalNumOutputChannels();
    
    // In case we have more outputs than inputs, this code clears any output
    // channels that didn't contain input data, (because these aren't
    // guaranteed to be empty - they may contain garbage).
    // This is here to avoid people getting screaming feedback
    // when they first compile a plugin, but obviously you don't need to keep
    // this code if your algorithm always overwrites all the output channels.
    for (auto i = totalNumInputChannels; i < totalNumOutputChannels; ++i)
        buffer.clear (i, 0, buffer.getNumSamples());

    // This is the place where you'd normally do the guts of your plugin's
    // audio processing...
    // Make sure to reset the state if your inner loop is processing
    // the samples and the outer loop is handling the channels.
    // Alternatively, you can process the samples with the channels
    // interleaved by keeping the same state.

    //update parameters based on slider positions.
    lfo.set_frequency(speedParameter->get());
    float depth { depthParameter->get() * oneMsDelay };
    int voiceCount { voicesParameter->get() };

    // assuming mono input for now
    auto* delayData = delayBuffer.getWritePointer(0);
    auto* channelData = buffer.getWritePointer(0);
    auto delayWritePointerOfFirstSample = delayWritePointer;

    //fill delay buffer
    for (int sample = 0; sample < buffer.getNumSamples(); ++sample) {
        delayData[delayWritePointer++] = channelData[sample];
        if (delayWritePointer == delayBuffer.getNumSamples()) {
            delayWritePointer = 0;
        }
    }

    //safe LFO state to reuse on both each channel
    auto lfoPhase = lfo.get_phase();

    //add delayed samples to the sample buffer.
    for (int channel = 0; channel < totalNumInputChannels; ++channel) {
        channelData = buffer.getWritePointer (channel);

        //start at same phase on both channels
        lfo.set_phase(lfoPhase);

        //process all samples
        for (int sample = 0; sample < buffer.getNumSamples(); ++sample) {
            channelData[sample] *= 0.5f;

            //change delay phase of all voices
            float nextPhase { static_cast<float>(lfo.generate_one_sample()) };

            //use the same calculation for all voices, but with different intensity.
            for (int voice = 0; voice < voiceCount; ++voice) {
                //calculate the index in the delayBuffer to be combined with a sample from the input buffer.
                //this could be more efficient.
                delayReadPointer[voice] = delayWritePointerOfFirstSample + sample - twentyMsDelay - (nextPhase * (voiceCount - voice)/voiceCount * depth);
                wrapAround(delayReadPointer[voice], delayBuffer);
                
                //add the delayed voice to the channel
                const int roundedReadPointer{ static_cast<int>(delayReadPointer[voice]) };
                channelData[sample] += 0.5f/voiceCount * delayData[roundedReadPointer];
            }
        }
    }
}

//prevent under and overflow by bound checking a pointer and wrapping if out of bound.
void ChorusAudioProcessor::wrapAround(float& samplePointer, AudioSampleBuffer& asBuffer) {
    if (samplePointer < 0.0) {
        samplePointer += asBuffer.getNumSamples();
    } else if (samplePointer >= asBuffer.getNumSamples()) {
        samplePointer -= asBuffer.getNumSamples();
    } 
}

//==============================================================================
bool ChorusAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

AudioProcessorEditor* ChorusAudioProcessor::createEditor()
{
    return new ChorusAudioProcessorEditor (*this);
}

//==============================================================================
void ChorusAudioProcessor::getStateInformation (MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.
}

void ChorusAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new ChorusAudioProcessor();
}
