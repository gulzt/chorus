/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
ChorusAudioProcessorEditor::ChorusAudioProcessorEditor (ChorusAudioProcessor& p) :
            AudioProcessorEditor (&p), processor (p),
            speedSlider {*p.getParameters()[0]},
            depthSlider {*p.getParameters()[1]},
            voicesSlider {*p.getParameters()[2]}
{
    // Make sure that before the constructor has finished, you've set the
    // editor's size to whatever you need it to be.

    //add components to the GUI
    addAndMakeVisible(speedLabel);
    addAndMakeVisible(depthLabel);
    addAndMakeVisible(voicesLabel);

    addAndMakeVisible(speedSlider);
    addAndMakeVisible(depthSlider);
    addAndMakeVisible(voicesSlider);

    const auto width = 240;
    setSize(width, width/4 * p.getParameters().size());
}

ChorusAudioProcessorEditor::~ChorusAudioProcessorEditor()
{
}

//==============================================================================
void ChorusAudioProcessorEditor::paint (Graphics& g)
{
    g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));
}

void ChorusAudioProcessorEditor::resized()
{
    // This is generally where you'll want to lay out the positions of any
    // subcomponents in your editor..
    // sets the position and size of the slider with arguments (x, y, width, height)
    const auto horMargin {16};
    const auto vertMargin {40}; 
    const auto vertOffset {48};
    const auto sliderWidth{200};
    const auto sliderHeight{24};

    speedLabel.setBounds(horMargin, 16, sliderWidth, sliderHeight);
    depthLabel.setBounds(horMargin, 16 + vertOffset, sliderWidth, sliderHeight);
    voicesLabel.setBounds(horMargin, 16 + 2*vertOffset, sliderWidth, sliderHeight);

    speedSlider.setBounds(horMargin, vertMargin, sliderWidth, sliderHeight);
    depthSlider.setBounds(horMargin, vertMargin + vertOffset, sliderWidth, sliderHeight);
    voicesSlider.setBounds(horMargin, vertMargin + 2*vertOffset, sliderWidth, sliderHeight);

}
