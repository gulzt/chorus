/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "PluginProcessor.h"
#include "ParameterSlider.h"

//==============================================================================
/**
*/
class ChorusAudioProcessorEditor  : public AudioProcessorEditor
{
public:
    ChorusAudioProcessorEditor (ChorusAudioProcessor&);
    ~ChorusAudioProcessorEditor();

    //==============================================================================
    void paint (Graphics&) override;
    void resized() override;

private:
    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    ChorusAudioProcessor& processor;

    // Labels and Controls on GUI
    Label speedLabel {"", "Speed (Hz)"};
    Label depthLabel {"", "Depth"};
    Label voicesLabel {"", "Voices"};
    ParameterSlider speedSlider;
    ParameterSlider depthSlider;
    ParameterSlider voicesSlider;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ChorusAudioProcessorEditor)
};
