#include "pch.h"
#include "../../../Source/Oscillator.h"
TEST(generate_one, hz_1_rate_4) {
	Oscillator osc;
	osc.set_frequency(1);
	osc.set_sample_rate(4);

	EXPECT_NEAR(osc.generate_one_sample(), 0.0, 0.001);
	EXPECT_NEAR(osc.generate_one_sample(), 1.0, 0.001);
	EXPECT_NEAR(osc.generate_one_sample(), 0.0, 0.001);
	EXPECT_NEAR(osc.generate_one_sample(), -1.0, 0.001);
	EXPECT_NEAR(osc.generate_one_sample(), 0.0, 0.001);
}

void generate_multiple_samples(Oscillator& osc, int times) {
	for (int i = 0; i < times; ++i) {
		osc.generate_one_sample();
	}
}
TEST(generate_one, hz_1_rate_44100) {
	Oscillator osc;
	osc.set_frequency(1);

	EXPECT_NEAR(osc.generate_one_sample(), 0.0, 0.001);
	
	generate_multiple_samples(osc, 44100/4 - 2);
	EXPECT_NEAR(osc.generate_one_sample(), 1.0, 0.001);

	generate_multiple_samples(osc, 44100 / 4 - 1);
	EXPECT_NEAR(osc.generate_one_sample(), 0.0, 0.001);

	generate_multiple_samples(osc, 44100 / 4 - 1);
	EXPECT_NEAR(osc.generate_one_sample(), -1.0, 0.001);

	generate_multiple_samples(osc, 44100 / 4 - 1);
	EXPECT_NEAR(osc.generate_one_sample(), 0.0, 0.001);
}